package api.definitions;

import api.WebServiceEndPoints;
import api.steps.GenericSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GenericDefinitions {

    @Steps
    GenericSteps genericSteps;

    private Response response;
    private Response responseNext;

//Method used for initiliasing the Map with http query parameters (key,value) pairs
    @Given("the client uses the following url parameters:")
    public void theFollowingUrlParameters(Map<String, String> urlParameters) {
        genericSteps.setDecorateUrl(urlParameters);
    }

    //Method that calls a web service endpoint configured in the enum class WebServiceEndpoints. The string used needs to be defines in the enum class
    @When("the client calls {string} endpoint")
    public void theUserCallsEndpoint(String webServiceEndpoint) {
        this.response = genericSteps.callEndpoint(webServiceEndpoint);
    }

    //Method that does the same thing as ""the client calls {string} endpoint" step but store the response in another Response object in order to do a comparison check between to JSON objects. This can be used for a baseline-under test checking approach where you first call the baseline endpoint, then under test endpoint and compare the two responses
    @When("the client calls {string} endpoint again")
    public void theUserCallsEndpointAgain(String webServiceEndpoint) {
        this.responseNext = genericSteps.callEndpoint(webServiceEndpoint);
    }

    //Same as "the client calls {string} endpoint" method but also checks that HTTP response code is 200
    @When("the client calls {string} endpoint with success")
    public void theUserCallsEndpointWithSuccess(String webServiceEndpoint) {
        theUserCallsEndpoint(webServiceEndpoint);
        theClientShouldReceiveHttpResponseCode(200);
    }

    // Same as "the client calls {string} endpoint again" method but also checks that HTTP response code is 200
    @When("the client calls {string} endpoint again with success")
    public void theUserCallsEndpointAgainWithSuccess(String webServiceEndpoint) {
        theUserCallsEndpointAgain(webServiceEndpoint);
        theClientShouldReceiveHttpResponseCode(200);
    }

    // Method used for doing a REST call for a method that requires a body
    @When("the client calls {string} endpoint with body {string}")
    public void theUserCallsEndpoint(String webServiceEndpoint, String body) {
        this.response = genericSteps.callEndpoint(webServiceEndpoint, body);
    }

    @Given("RestAPI is up and running")
    public void restApiIsUpAndRunning() {
    }

    @And("Verify response body contains my user name {string}")
    public void verifyResponseBodyContainsMyUserName(String username) {
        String login = response.getBody().jsonPath().get("login");
        Assert.assertEquals(username,login);
    }

    @And("verify response body contains my github repo URL {string}")
    public void verifyResponseBodyContainsMyGithubRepoURL(String gutHubURL) {
        String url = response.getBody().jsonPath().getString("url");
        Assert.assertEquals(gutHubURL,url);
    }

    @And("verify response body contains message {string}")
    public void verifyResponseBodyContainsMessageNotFound(String errorMsg) {
       String message = response.getBody().jsonPath().getString("message");
       Assert.assertEquals(message,errorMsg);

    }


      //Method that check the response object that has status code XXX (type int)
      @Then("the client should receive an HTTP {int} response code")
      public void theClientShouldReceiveHttpResponseCode(int httpCode) {
        assertThat(response.getStatusCode(), equalTo(httpCode));
}

      }