package api;

public enum WebServiceEndPoints {
    GITHUB_USER("https://api.github.com/users/rrifatra", "GET"),
    WRONG_USER("https://api.github.com/users/rrifatra123", "GET");

    private final String url;
    private final String method;

    WebServiceEndPoints(String url, String method) {
        this.url = url;
        this.method = method;
    }

    public String getUrl() {
        return url;
    }
    public String getMethod() {return method;}

}
