Feature: Basic checks for github user end point
  - required username
  - required github url

  @automated
  @smoke
  Scenario: Success Scenario - Verify GitHUB user end point returns valid status code and response body for valid user
    Given RestAPI is up and running
    When the client calls 'GITHUB_USER' endpoint
    Then the client should receive an HTTP 200 response code
    And Verify response body contains my user name 'rrifatra'
    And verify response body contains my github repo URL 'https://api.github.com/users/rrifatra'

  @automated
  @smoke
  Scenario: Error Scenario - Verify GitHUB user end point returns error 404 status code for invalid user
    Given RestAPI is up and running
    When the client calls 'WRONG_USER' endpoint
    Then the client should receive an HTTP 404 response code
    And verify response body contains message 'Not Found'

  @ignore
  Scenario: Error Scenario - Verify GitHUB user end point returns internal error 500 status code when API is down
     Given RestAPI is up and running
     When the client calls 'WRONG_USER' endpoint
     Then the client should receive an HTTP 500 response code
     And verify response body contains message 'Not Found'

  @ignore
  Scenario: Error Scenario - Verify GitHUB user end point returns bad request 400 status code when bad request is sent
    Given RestAPI is up and running
    When the client calls 'WRONG_USER' endpoint
    Then the client should receive an HTTP 400 response code
    And verify response body contains message 'Not Found'

  @ignore
  Scenario: Error Scenario - Verify GitHUB user end point returns forbidden 403 when access token is invalid
    Given RestAPI is up and running
    When the client calls 'WRONG_USER' endpoint
    Then the client should receive an HTTP 403 response code
    And verify response body contains message 'Not Found'