##### Automated Regression
- Based on Serenity+Cucumber4
- Capabilities
  - run predefined suites (smoke,automated,complete feature)
  - do REST API (GET/POST)calls to predefined endpoints
  - check response code, headers,response body using jsonPath

- Structure
  - src/test/java/api - Test runners and supporting code
  - src/test/resources/features - Feature files

- API Details:
  - API used for execution is "https://api.github.com/users/" available on the site "www.any-api.com"
  - Response codes tested are "200, 404, 500, 403, 400"
  - Response body verified for the user and the repo url.
  
- How to run:
  - Pre-requisites: maven, java8 or greater
  - JUnit:
    - go to **src/test/java/api/** and run class **CucumberTestSuite.java** (will run all testcases with @automated tag by default)
    - you can modify the tags you want to run from @CucumberOptions inside the class
  - Maven:
    - run command from base project: **mvn integration-test** (will run all testcases with @automated tag by default)
    - if you want to run different tags: ** mvn clean verify -Dcucumber.options="--tags @test"**
    - html report is generated when running previous commands - open target/site/serenity/index.html after run
  - Tags:
    - @ignore: These tests are not executing as the api is not returning the status codes and hence they are marked as ignored
    - @automated: Use this tag to run all the tests that have been automated and executed
    - @smoke: These are scenarios that can be executed as part of smoke test

To run the pipeline directly, please go to CI/CD ==> Pipeline ==> Run pipeline.
The .gitlab-ci.yml file is a YAML file where you find specific instructions for GitLab CI/CD.



